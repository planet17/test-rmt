up: ## Up
	docker-compose up -d
	docker-compose exec rmt-app bash -c 'composer update'

in: ## Go inside app container
	docker-compose exec rmt-app bash
