create table customers
(
    id         int auto_increment,
    email      varchar(64) not null,
    phone      varchar(16) null,
    first_name varchar(32) null,
    last_name  varchar(32) null,
    password   varchar(32) not null,
    constraint customers_pk
        primary key (id)
);

create unique index customers_email_u_index
    on customers (email);
