<?php

declare(strict_types=1);
/**
 * @var array $errors
 */
if ($errors ?? []) { ?>
  <div class="list-error">
      <?php
      foreach ($errors as $error) { ?>
        <h1 class="error">
            <?= $error ?>
        </h1>
          <?php
      } ?>
  </div>
    <?php
} ?>

