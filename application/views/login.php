<?php

declare(strict_types=1);

require_once __DIR__ . '/parts/nav.php';
/**
 * @var \App\Models\Forms\FormLogin $model
 */
require_once __DIR__ . '/parts/errors.php';
?>
<form method="POST">
    <div>
      <label>
        <span>Email</span>
        <input type="email" name="email" required="required" value="<?= $model->getEmail() ?>" placeholder="email@gmail.com" minlength="6" maxlength="64">
      </label>
    </div>
    <div>
      <label>
        <span>Password</span>
        <input type="password" name="password" required="required" value="<?= $model->getPassword() ?>" placeholder="Password" minlength="6" maxlength="32">
      </label>
    </div>
    <div>
        <button type="submit">Login</button>
    </div>
</form>
