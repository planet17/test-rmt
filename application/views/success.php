<?php

declare(strict_types=1);

require_once __DIR__ . '/parts/nav.php';
/**
 * @var \App\Models\Forms\FormLogin $model
 * @var array $messages
 */
if ($messages) { ?>
  <div class="list-error">
      <?php foreach ($messages as $message) { ?>
        <h1 class="message">
            <?= $message ?>
        </h1>
      <?php } ?>
  </div>
<?php } ?>

