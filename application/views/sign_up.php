<?php

declare(strict_types=1);

require_once __DIR__ . '/parts/nav.php';
require_once __DIR__ . '/parts/errors.php';
/**
 * @var \App\Models\Forms\FormSignUp $model
 */
?>
<form method="POST">
  <div>
    <label>
        <span>Email</span>
        <input type="email" name="email" required="required" value="<?= $model->getEmail() ?>" placeholder="email@gmail.com" minlength="6" maxlength="64">
    </label>
  </div>
  <div>
    <label>
        <span>Firstname</span>
        <input type="text" name="first_name" value="<?= $model->getFirstName() ?>" placeholder="Firstname" minlength="2">
    </label>
  </div>
  <div>
    <label>
        <span>Lastname</span>
        <input type="text" name="last_name" value="<?= $model->getLastName() ?>" placeholder="Lastname" minlength="2">
    </label>
  </div>
  <div>
    <label>
        <span>Phone</span>
        <input type="text" name="phone" value="<?= $model->getPhone() ?>" placeholder="Phone number" minlength="5">
    </label>
  </div>
  <div>
    <label>
        <span>Password</span>
        <input type="password" name="password" required="required" value="<?= $model->getPassword() ?>" placeholder="Password" minlength="6" maxlength="32">
    </label>
  </div>
  <div>
    <button type="submit">Sign Up</button>
  </div>
</form>
