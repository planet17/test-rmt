<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

(new \App\Core\App(
        new \App\Core\Router,
        new \App\Core\DbConnection(
            getenv('DB_HOSTNAME'),
            getenv('DB_MAIN_DATABASE'),
            getenv('DB_MAIN_USERNAME'),
            getenv('DB_MAIN_PASSWORD'),
        ),
    )
)->execute();
