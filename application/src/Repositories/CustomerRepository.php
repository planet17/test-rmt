<?php

declare(strict_types=1);

namespace App\Repositories;

use PDO;
use PDOStatement;

class CustomerRepository extends MySqlRepository
{
    public function getTableName(): string
    {
        return 'customers';
    }

    public function find(int $id): ?Customer
    {
        $statement = $this->executeSelect('SELECT * from ' . $this->getTableName() . ' WHERE id = ' . $id . ' LIMIT 1');
        if (!$statement) {
            return null;
        }

        return $this->extractOne($statement);
    }

    public function findOneByEmail(string $email): ?Customer
    {
        $raw = 'SELECT * from ' . $this->getTableName() . ' WHERE email LIKE \'' . $email . '\' LIMIT 1';
        $statement = $this->executeSelect($raw);
        if (!$statement) {
            return null;
        }

        return $this->extractOne($statement);
    }

    public function add(Customer $customer): void
    {
        $raw = 'INSERT INTO ' . $this->getTableName(
            ) . ' (email, password, first_name, last_name, phone) VALUES (:email, :password, :first_name, :last_name, :phone)';

        $data = [
            'email' => $customer->getEmail(),
            'password' => $customer->getPasswordHashed(),
            'first_name' => $customer->getFirstName() ?? '',
            'last_name' => $customer->getLastName() ?? '',
            'phone' => $customer->getPhone() ?? '',
        ];

        if (!$this->executeInsert($raw, $data)) {
            throw new \RuntimeException('Something wrong on insert new customer');
        }
    }

    protected function transformToEntity(?array $data): ?Customer
    {
        if (!$data) {
            return null;
        }

        $customer = new Customer;
        $customer->setId($data['id'] ?? null);
        $customer->setEmail($data['email'] ?? null);
        $customer->setPasswordHashed($data['password'] ?? null);
        $customer->setFirstName($data['first_name'] ?? null);
        $customer->setLastName($data['last_name'] ?? null);
        $customer->setPhone($data['phone'] ?? null);

        return $customer;
    }
}
