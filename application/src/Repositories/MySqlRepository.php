<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Core\App;
use App\Core\DbConnection;
use PDO;
use PDOStatement;

abstract class MySqlRepository
{
    private DbConnection $connection;

    public function __construct()
    {
        $this->connection = App::getStorage();
    }

    protected function getConnection(): DbConnection
    {
        return $this->connection;
    }

    protected function extractOne(PDOStatement $statement): mixed
    {
        return $this->transformToEntity($statement->getIterator()->current());
    }

    protected function executeSelect(string $raw): false|PDOStatement
    {
        return $this->getConnection()
            ->getConnection()
            ->query($raw);
    }

    protected function executeInsert(string $raw, array $data): bool
    {
        $connection = $this->getConnection()->getConnection();
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $connection->prepare($raw)->execute($data);
    }

    abstract protected function transformToEntity(?array $data): mixed;
}
