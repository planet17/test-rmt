<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Core\Exceptions\RedirectException;
use App\Models\Forms\FormSignUp;
use App\Models\PasswordHasher;
use App\Models\Registration;
use App\Repositories\CustomerRepository;
use RuntimeException;

class SignUp extends ControllerGui
{
    /**
     * @throws RedirectException
     */
    public function __invoke(): void
    {
        $form = new FormSignUp;
        $errors = [];

        try {
            if ($form->isValid()) {
                (new Registration(new CustomerRepository, new PasswordHasher))->handle($form);
                $this->redirect('/');
            }
        } catch (RuntimeException $exception) {
            $errors[] = $exception->getMessage();
        }

        $this->render('sign_up', ['model' => $form, 'errors' => $errors]);
    }
}
