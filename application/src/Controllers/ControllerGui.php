<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Core\Exceptions\RedirectException;
use App\Core\Exceptions\ViewNotFoundException;

abstract class ControllerGui
{
    abstract public function __invoke(): void;

    public function render(string $viewPath, array $data = []): void
    {
        $filePathView = $this->preparePathFullView($viewPath);
        if ($data) {
            extract($data);
        }

        include_once $filePathView;
    }

    private function preparePathFullView(string $viewPath): string
    {
        $root = dirname(__DIR__, 2);
        $dirView = $root . '/views';
        $filename = $dirView . '/' . $viewPath . '.php';

        if (!file_exists($filename)) {
            throw new ViewNotFoundException($viewPath);
        }

        return $filename;
    }

    /**
     * @throws RedirectException
     */
    protected function redirect(string $url): void
    {
        throw new RedirectException($url, RedirectException::DEFAULT_STATUS_SUCCESS);
    }
}
