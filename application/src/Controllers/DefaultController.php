<?php

declare(strict_types=1);

namespace App\Controllers;

class DefaultController extends ControllerGui
{
    public function __invoke(): void
    {
        $this->render('default');
    }
}
