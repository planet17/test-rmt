<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Models\Exceptions\LoginFailedException;
use App\Models\Exceptions\LoginSucceedException;
use App\Models\Forms\FormLogin;
use App\Models\Login as LoginModel;
use App\Models\PasswordHasher;
use App\Repositories\CustomerRepository;

class Login extends ControllerGui
{
    public function __invoke(): void
    {
        $form = new FormLogin;
        $messages  = [];

        if ($form->isValid()) {
            try {
                (new LoginModel(new CustomerRepository, new PasswordHasher))->handle($form);
            } catch (LoginFailedException $e) {
                $messages[] = 'Login failed';
            } catch (LoginSucceedException $e) {
                $this->render('success', ['messages' => ['Congratulation! Login succeed']]);
                return;
            }
        }

        $this->render('login', ['model' => $form, 'errors' => $messages]);
    }
}
