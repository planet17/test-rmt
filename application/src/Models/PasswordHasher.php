<?php

declare(strict_types=1);

namespace App\Models;

use SensitiveParameter;

class PasswordHasher
{
    public function hash(#[SensitiveParameter] string $origin): string
    {
        return md5($origin);
    }
}
