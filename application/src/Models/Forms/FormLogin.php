<?php

declare(strict_types=1);

namespace App\Models\Forms;

class FormLogin extends Form
{
    private ?string $email = null;
    private ?string $password = null;


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function collect(): void
    {
        $this->email = $_POST['email'] ?? null;
        $this->password = $_POST['password'] ?? null;
    }

    public function isValid(): bool
    {
        if (!$this->email) {
            return false;
        }

        if (!$this->password) {
            return false;
        }

        if (strlen($this->email) < 6 || strlen($this->email) > 64) {
            return false;
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if (strlen($this->password) < 6 || strlen($this->password) > 32) {
            return false;
        }

        return true;
    }
}
