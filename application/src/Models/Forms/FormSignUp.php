<?php

declare(strict_types=1);

namespace App\Models\Forms;

class FormSignUp extends Form
{
    private ?string $firstName = null;
    private ?string $lastName = null;
    private ?string $email = null;
    private ?string $phone= null;
    private ?string $password = null;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function collect(): void
    {
        $this->email = $_POST['email'] ?? null;
        $this->password = $_POST['password'] ?? null;
        $this->firstName = $_POST['first_name'] ?? null;
        $this->lastName = $_POST['last_name'] ?? null;
        $this->phone = $_POST['phone'] ?? null;
    }

    public function isValid(): bool
    {
        if (!$this->email) {
            return false;
        }

        if (!$this->password) {
            return false;
        }

        if (strlen($this->email) < 6 || strlen($this->email) > 64) {
            return false;
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if (strlen($this->password) < 6 || strlen($this->password) > 32) {
            return false;
        }

        if ($this->firstName && (strlen($this->firstName) < 2 || strlen($this->firstName) > 32)) {
            return false;
        }

        if ($this->lastName && (strlen($this->lastName) < 2 || strlen($this->lastName) > 32)) {
            return false;
        }

        if ($this->phone && (strlen($this->phone) < 5 || strlen($this->phone) > 16)) {
            return false;
        }

        return true;
    }
}
