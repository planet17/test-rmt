<?php

declare(strict_types=1);

namespace App\Models\Forms;

abstract class Form
{
    public function __construct(
    ) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->collect();
        }
    }

    abstract public function collect(): void;

    abstract public function isValid(): bool;
}
