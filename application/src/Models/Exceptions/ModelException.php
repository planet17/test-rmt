<?php

declare(strict_types=1);

namespace App\Models\Exceptions;

class ModelException extends \Exception
{
}
