<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Forms\FormSignUp;
use App\Repositories\Customer;
use App\Repositories\CustomerRepository;

readonly class Registration
{
    public function __construct(
        private CustomerRepository $customerRepository,
        private PasswordHasher $hasher,
    ) {
    }

    public function handle(FormSignUp $form): void
    {
        $item = $this->customerRepository->findOneByEmail($form->getEmail());
        if ($item) {
            throw new \RuntimeException('Email already in use');
        }

        $item = new Customer;
        $item->setEmail($form->getEmail());
        $item->setPhone($form->getPhone());
        $item->setPasswordHashed($this->hasher->hash($form->getPassword()));
        $item->setFirstName($form->getFirstName());
        $item->setLastName($form->getLastName());
        $this->customerRepository->add($item);
    }
}
