<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Exceptions\LoginFailedException;
use App\Models\Exceptions\LoginSucceedException;
use App\Models\Forms\FormLogin;
use App\Repositories\CustomerRepository;

readonly class Login
{
    public function __construct(
        private CustomerRepository $customerRepository,
        private PasswordHasher $hasher,
    ) {
    }

    /**
     * @throws LoginSucceedException
     * @throws LoginFailedException
     */
    public function handle(FormLogin $form): void
    {
        $item = $this->customerRepository->findOneByEmail($form->getEmail());
        if (!$item) {
            throw new \RuntimeException('Not found user');
        }

        if ($this->hasher->hash($form->getPassword()) !== $item->getPasswordHashed()) {
            throw new LoginFailedException;
        }

        throw new LoginSucceedException;
    }
}
