<?php

declare(strict_types=1);

namespace App\Core;

use PDO;

class DbConnection
{
    private PDO $db;

    public function __construct(
        string $host,
        string $db,
        string $user,
        string $password,
    ) {
        $this->db = new PDO('mysql:host=' . $host . ';dbname=' . $db, $user, $password);
    }

    public function getConnection(): PDO
    {
        return $this->db;
    }
}
