<?php

declare(strict_types=1);

namespace App\Core;

use App\Core\Exceptions\RedirectException;

class App
{
    private static DbConnection $storage;

    public function __construct(
        private readonly Router $router,
        DbConnection $connectionStorage,
    ) {
        self::$storage = $connectionStorage;
    }

    public function execute(): void
    {
        try {
            $currentUrl = $_SERVER['REQUEST_URI'] ?? '/';
            $controller = $this->router->match($currentUrl);
            $controller();
        } catch (RedirectException $redirect) {
            header('HTTP/1.1 ' . $redirect->getStatus() . ' Moved Permanently');
            header('Location: ' . $redirect->getUrl());
            die;
        }
    }

    public static function getStorage(): DbConnection
    {
        return self::$storage;
    }
}
