<?php

declare(strict_types=1);

namespace App\Core\Exceptions;

use Throwable;

class RedirectException extends \Exception
{
    public const DEFAULT_STATUS_SUCCESS = 200;
    public const DEFAULT_STATUS_MOVED = 301;

    public function __construct(
        private readonly string $url,
        private readonly int $status = self::DEFAULT_STATUS_MOVED,
        string $message = '',
        int $code = 0,
        ?Throwable $previous = null,
    )
    {
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
}
