<?php

declare(strict_types=1);

namespace App\Core;

use App\Controllers\ControllerGui;
use App\Controllers\DefaultController;
use App\Controllers\Login;
use App\Controllers\SignUp;
use App\Core\Exceptions\InvalidRouteConfigurationException;
use App\Core\Exceptions\RedirectException;

class Router
{
    public function rules(): array
    {
        return [
            '/login' => Login::class,
            '/sign-up' => SignUp::class,
            '/' => DefaultController::class,
        ];
    }

    /**
     * @throws RedirectException
     */
    public function match(string $url): ControllerGui
    {
        $className = $this->resolveControllerClass($url);

        $instance = new $className;
        if (!($instance instanceof ControllerGui)) {
            throw new InvalidRouteConfigurationException('Should be provided only controllers');
        }

        return $instance;
    }

    /**
     * @throws RedirectException
     */
    private function resolveControllerClass($needle): string
    {
        foreach ($this->rules() as $rule => $controllerClassName) {
            if ($rule === $needle) {
                return $controllerClassName;
            }
        }

        throw new RedirectException('/', 404);
    }
}
